{ lib, pkgs, ... }:

let 
  theme = import ./themes { inherit pkgs; };
  stripHash = (s: builtins.substring 1 (-1) s);
in {
  imports = [
    ./xsession
    ./packages
    ./programs
    ./services
  ];

  home = {
    sessionVariables = {
      PATH="$HOME/.yarn/bin/:$PATH";
      USE_NIX2_COMMAND=1;
      EDITOR="emacs";
      PAGER="most";
    };
    keyboard = {
      layout = "us,ru";
      options = [ "grp:caps_toggle" ];
    };
  };

  gtk = {
    enable = true;
    iconTheme = {
      name = "Paper";
      package = pkgs.paper-icon-theme;
    };
    theme = rec {
      name = "Materia-Custom";
      package = (pkgs.materia-theme.overrideAttrs (base: {

        buildInputs = base.buildInputs ++ [ pkgs.sassc pkgs.inkscape pkgs.optipng ];

        installPhase = ''
          patchShebangs *.sh scripts/*.sh src/*/*.sh
          sed -i install.sh \
            -e "s|if .*which gnome-shell.*;|if true;|" \
            -e "s|CURRENT_GS_VERSION=.*$|CURRENT_GS_VERSION=${lib.versions.majorMinor pkgs.gnome3.gnome-shell.version}|"
          sed -i change_color.sh \
            -e "s|\$HOME/\.themes|$out/share/themes|"
          ./change_color.sh -o '${name}' <(echo -e "BG=${stripHash theme.colors.background.primary}\n \
                                                       FG=${stripHash theme.colors.text.primary}\n \
                                                       MATERIA_VIEW=${stripHash theme.colors.background.primary}\n \
                                                       MATERIA_SURFACE=${stripHash theme.colors.background.secondary}\n \
                                                       HDR_BG=${stripHash theme.colors.background.secondary}\n \
                                                       HDR_FG=${stripHash theme.colors.text.primary}\n \
                                                       SEL_BG=${stripHash theme.colors.background.selection}\n \
                                                       ROUNDNESS=0\n \
                                                       MATERIA_STYLE_COMPACT=True")
          rm $out/share/themes/*/COPYING
        '';

      }));
    };
    gtk3.extraConfig = {
      gtk-application-prefer-dark-theme = theme.isDark;
      # gtk-cursor-theme-name = "Paper";
    };
  };

  programs = {

    home-manager.enable = true;
    home-manager.path = https://github.com/rycee/home-manager/archive/master.tar.gz;

  };

  home.extraProfileCommands = ''
    if [[ -d "$out/share/applications" ]] ; then
      ${pkgs.desktop-file-utils}/bin/update-desktop-database $out/share/applications
    fi
  '';
}
